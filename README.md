# PlanA DevOps Challenge 1

## Running the app locally

1. python3 -m venv venv
2. Install Flask: `pip install Flask`
3. Run the app: `flask run`

The application should be available at `localhost:8080`.

## Testing the Deployment manifest on AKS

```sh
$ curl http://52.224.26.216
{"timestamp": "2022-03-01 05:33:53.956584", "hostname": "visitor-api-64c6775f89-8ccbn", "engine": "Python3.10.2", "visitor ip": "X.X.X.X"}
```

Where X.X.X.X is my router's IP address when using externalTrafficPolicy: Local in the service type LoadBalancer.

## Testing on Azure Container Instances

```sh
$ az container create \
 --resource-group cloudmaster-eastus \
 --name visitor-api \
 --image aramirezg/visitor:v1 \
 --dns-name-label visitor-api \
 --ports 8080

$ curl http://52.226.57.113:8080
{"timestamp": "2022-03-01 05:15:44.458017", "hostname": "SandboxHost-637817085300290231", "engine": "Python3.10.2", "visitor ip": "Y.Y.Y.Y"}
```

IP address in that case should be LB's IP addres.

## Using port-forward

```sh
$ kubectl port-forward svc/visitor-api 8080:80
Forwarding from 127.0.0.1:8080 -> 8080
Forwarding from [::1]:8080 -> 8080
```
