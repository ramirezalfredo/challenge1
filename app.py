import json
import datetime
import socket
import platform

from flask import Flask, jsonify, request

ct = str(datetime.datetime.now())
host_name = socket.gethostname() 

app = Flask(__name__)


@app.route('/health')
def health():
    return jsonify({'status': 'UP'})


@app.route('/', methods=['GET', 'POST'])
def visitor():
    if request.method == 'GET':
        response = app.response_class(
            response=json.dumps(
                {
                    "timestamp": ct,
                    "hostname": host_name,
                    "engine": "Python"+platform.python_version(),
                    "visitor ip": request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
                }
            ),
            status=200,
            mimetype='application/json'
        )
    else:
        raise Exception('Unsupported HTTP request type.')
    return response


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8080') #, debug=True)