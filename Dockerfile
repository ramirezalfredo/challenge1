FROM python:3.10-alpine
LABEL maintainer="Alfredo Ramirez"

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 8080

# command to run on container start
CMD [ "python", "app.py" ]